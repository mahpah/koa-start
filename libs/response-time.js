/**
 * Example of middleware. A function that return a genarator.
 * Of course you can use generator itself. But for some mysterious
 * reason, this is recommended practice
 * @param  {string} headerName some parameter, any things...
 * @return {generator}            actual middleware
 */
export function responseTime(headerName) {
  return function* (next) {
    let start = +new Date();
    yield next;
    let time = +new Date() - start;
    this.set(headerName, time + 'ms');
  };
}
