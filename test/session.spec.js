import 'babel-polyfill';
import request from 'co-supertest';
import koa from 'koa';
import session from '../libs/session';
import 'co-mocha';

const app = koa();
app.keys = ['ahihi'];

app.use(session());

app.use(function*() {
  switch (this.request.url) {
    case '/setname':
      this.session.userName = 'Ha Pham';
      return;
  }

});

let server = app.listen();

/**
 * Test case
 */

describe('Session middleware', () => {
  let agent;
  before(() => {
    agent = request.agent(server);
  });

  it('should set name in session object', function* () {
    yield agent
      .get('/setname')
      .expect(200)
      .end();
  });

});
