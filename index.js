import koa from 'koa';
import { responseTime } from './libs/response-time.js';
import routes from './routes';

let app = koa();
app.use(responseTime('Koa-Response-Time'));
app.use(routes());

app.listen(process.env.PORT || 3000);
