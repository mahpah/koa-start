import router from 'koa-router';
let route = router();

route.get('/', function*() {
  console.log('ad');
  this.body = 'Hello';
});

route.get('/date', function*() {
  this.body = new Date();
});

/**
 * prevent losing context
 */
export default route.routes.bind(route);
